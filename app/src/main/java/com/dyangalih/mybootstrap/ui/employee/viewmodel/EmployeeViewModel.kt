package com.dyangalih.mybootstrap.ui.employee.viewmodel

import androidx.lifecycle.*
import com.dyangalih.mybootstrap.data.entities.Employee
import com.dyangalih.mybootstrap.data.entities.EmployeeList
import com.dyangalih.mybootstrap.data.remote.request.EmployeeStoreRequest
import com.dyangalih.mybootstrap.data.repository.EmployeeRepository
import com.dyangalih.mybootstrap.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class EmployeeViewModel
@Inject constructor(
    private val employeeRepository: EmployeeRepository
) : ViewModel() {
    private val _employeeList = MutableLiveData<Resource<EmployeeList>>()
    val employeeList: LiveData<Resource<EmployeeList>> get() = _employeeList

    private val _employee = MutableLiveData<Resource<Employee>>()
    val employee: LiveData<Resource<Employee>> get() = _employee

    fun getEmployee(){
        viewModelScope.launch {
            _employeeList.postValue(Resource.loading(null))
            employeeRepository.getEmployee().let {
                _employeeList.postValue(it)
            }
        }
    }

    fun create(employeeStoreRequest: EmployeeStoreRequest){
        viewModelScope.launch {
            _employee.postValue(Resource.loading(null))
            employeeRepository.create(employeeStoreRequest).let {
                _employee.postValue(it)
            }
        }
    }

    fun delete(id: Int){
        viewModelScope.launch {
            _employee.postValue(Resource.loading(null))
            employeeRepository.delete(id).let {
                _employee.postValue(it)
                getEmployee()
            }
        }
    }
}