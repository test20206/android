package com.dyangalih.mybootstrap.ui.sample.view

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.dyangalih.mybootstrap.databinding.ActivitySampleBinding
import com.dyangalih.mybootstrap.ui.sample.adapter.SampleAdapter
import com.dyangalih.mybootstrap.ui.sample.viewmodel.SampleViewModel
import com.dyangalih.mybootstrap.utils.Resource
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SampleActivity : AppCompatActivity(), SampleAdapter.SampleItemListener {

    private val sampleViewModel: SampleViewModel by viewModels()
    private lateinit var binding: ActivitySampleBinding
    private lateinit var adapter: SampleAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySampleBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupRecyclerView()
        setupObserver()
    }

    private fun setupRecyclerView() {
        adapter = SampleAdapter(this)
        binding.sampleRv.layoutManager = LinearLayoutManager(this)
        binding.sampleRv.adapter = adapter
    }

    override fun onClickedItem(itemId: Int) {
        TODO("Not yet implemented")
    }

    private fun setupObserver() {
        sampleViewModel.sample.observe(this@SampleActivity,
            {
                when (it.status) {
                    Resource.Status.SUCCESS -> {
                        binding.progressBar.visibility = View.GONE
                        if (!it.data.isNullOrEmpty()) adapter.setItems(ArrayList(it.data))
                    }
                    Resource.Status.ERROR -> Toast.makeText(
                        this@SampleActivity,
                        it.message,
                        Toast.LENGTH_LONG
                    ).show()

                    Resource.Status.LOADING -> binding.progressBar.visibility = View.VISIBLE
                }
            }
        )
    }
}