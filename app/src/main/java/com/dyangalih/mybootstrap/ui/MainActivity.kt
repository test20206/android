package com.dyangalih.mybootstrap.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.dyangalih.mybootstrap.R
import com.dyangalih.mybootstrap.ui.employee.view.CreateActivity

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val intent = Intent(this@MainActivity, CreateActivity::class.java)
        startActivity(intent)
        finish()
    }
}