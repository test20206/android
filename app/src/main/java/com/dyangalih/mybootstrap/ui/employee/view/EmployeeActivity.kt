package com.dyangalih.mybootstrap.ui.employee.view

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.dyangalih.mybootstrap.databinding.ActivityEmployeeBinding
import com.dyangalih.mybootstrap.ui.employee.adapter.EmployeeAdapter
import com.dyangalih.mybootstrap.ui.employee.viewmodel.EmployeeViewModel
import com.dyangalih.mybootstrap.utils.Resource
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class EmployeeActivity : AppCompatActivity(), EmployeeAdapter.EmployeeItemListener {
    private val employeeViewModel: EmployeeViewModel by viewModels()
    private lateinit var binding: ActivityEmployeeBinding
    private lateinit var adapter: EmployeeAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEmployeeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupRv()
        setupObserver()
        employeeViewModel.getEmployee()
    }

    private fun setupRv() {
        adapter = EmployeeAdapter(this)
        binding.employeeRv.layoutManager = LinearLayoutManager(this)
        binding.employeeRv.adapter = adapter
    }

    private fun setupObserver() {
        employeeViewModel.employeeList.observe(this@EmployeeActivity, {
            when (it.status) {
                Resource.Status.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE
                    if(!it.data?.results.isNullOrEmpty()) adapter.setItems(ArrayList(it.data?.results))
                }

                Resource.Status.ERROR->{
                    Toast.makeText(this@EmployeeActivity, it.message, Toast.LENGTH_LONG).show()
                    binding.progressBar.visibility = View.GONE
                }

                Resource.Status.LOADING -> binding.progressBar.visibility = View.VISIBLE
            }
        })
    }

    override fun onBtnEditClicked(itemId: Int) {
        val intent = Intent(this@EmployeeActivity, CreateActivity::class.java)
        intent.putExtra("ID", itemId.toString())
        startActivity(intent)
    }

    override fun onBtnDeleteClicked(itemId: Int) {
        employeeViewModel.delete(itemId)
    }
}