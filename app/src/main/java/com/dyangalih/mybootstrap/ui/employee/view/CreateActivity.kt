package com.dyangalih.mybootstrap.ui.employee.view

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.dyangalih.mybootstrap.data.remote.request.EmployeeStoreRequest
import com.dyangalih.mybootstrap.databinding.ActivityCreateBinding
import com.dyangalih.mybootstrap.ui.employee.viewmodel.EmployeeViewModel
import com.dyangalih.mybootstrap.utils.Resource
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

@AndroidEntryPoint
class CreateActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var binding: ActivityCreateBinding
    private val employeeViewModel: EmployeeViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCreateBinding.inflate(layoutInflater)
        binding.btnList.setOnClickListener(this)
        binding.btnSubmit.setOnClickListener(this)
        setContentView(binding.root)
        setupObserver()
    }

    private fun setupObserver() {
        employeeViewModel.employee.observe(this@CreateActivity, {
            when(it.status){
                Resource.Status.SUCCESS -> successCreate()
                Resource.Status.ERROR -> {
                    Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                    binding.progressBar.visibility = View.GONE
                }
                Resource.Status.LOADING -> binding.progressBar.visibility = View.VISIBLE
            }
        })
    }

    private fun successCreate(){
        binding.etAge.text.clear()
        binding.etName.text.clear()
        binding.etSalary.text.clear()
        binding.progressBar.visibility = View.GONE
        Toast.makeText(this, "Save Data Success", Toast.LENGTH_LONG).show()
    }

    override fun onClick(v: View?) {
        Timber.d(v?.id.toString())
        Timber.d(binding.btnList.id.toString())
        when(v?.id){
            binding.btnSubmit.id -> {
                val employeeStoreRequest = EmployeeStoreRequest(
                    binding.etName.text.toString(),
                    binding.etSalary.text.toString().toFloat(),
                    binding.etAge.text.toString().toInt()
                )

                employeeViewModel.create(employeeStoreRequest)
            }
            binding.btnList.id -> {
                val intent = Intent(this@CreateActivity, EmployeeActivity::class.java)
                startActivity(intent)
                finish()
            }
        }
    }
}