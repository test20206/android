package com.dyangalih.mybootstrap.ui.sample.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dyangalih.mybootstrap.data.entities.SampleTable
import com.dyangalih.mybootstrap.databinding.ItemDataBinding

class SampleAdapter(private val listener: SampleItemListener) :
    RecyclerView.Adapter<SampleAdapter.SampleViewHolder>() {
    private val items = ArrayList<SampleTable>()

    interface SampleItemListener{
        fun onClickedItem(itemId: Int);
    }

    fun setItems(items: ArrayList<SampleTable>){
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    class SampleViewHolder(private val itemDataBinding: ItemDataBinding, private val listener: SampleItemListener) :
        RecyclerView.ViewHolder(itemDataBinding.root), View.OnClickListener {

        private lateinit var item: SampleTable

        init {
            itemDataBinding.root.setOnClickListener(this)
        }

        fun bin(sample: SampleTable) {
            itemDataBinding.txtName.text = sample.name
            this.item = sample
        }

        override fun onClick(v: View?) {
            listener.onClickedItem(this.item.id)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SampleViewHolder =
        SampleViewHolder(
            ItemDataBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ), listener
        )

    override fun onBindViewHolder(holder: SampleViewHolder, position: Int) {
        holder.bin(items[position])
    }

    override fun getItemCount(): Int = items.size
}