package com.dyangalih.mybootstrap.ui.employee.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dyangalih.mybootstrap.R
import com.dyangalih.mybootstrap.data.entities.Employee
import com.dyangalih.mybootstrap.data.entities.EmployeeList
import com.dyangalih.mybootstrap.databinding.ItemEmployeeBinding

class EmployeeAdapter(private val listen: EmployeeItemListener) :
    RecyclerView.Adapter<EmployeeAdapter.EmployeeViewHolder>() {

    private val items = ArrayList<Employee>()

    interface EmployeeItemListener {
        fun onBtnEditClicked(itemId: Int)
        fun onBtnDeleteClicked(ItemId: Int)
    }

    fun setItems(items: ArrayList<Employee>){
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    class EmployeeViewHolder(
        private val itemEmployeeBinding: ItemEmployeeBinding,
        private val listener: EmployeeItemListener
    ) : RecyclerView.ViewHolder(itemEmployeeBinding.root), View.OnClickListener {

        private var id: Int = 0

        init {
            itemEmployeeBinding.btnDelete.setOnClickListener(this)
            itemEmployeeBinding.btnEdit.setOnClickListener(this)
        }

        fun bind(employee: Employee) {
            itemEmployeeBinding.txtId.text = employee.id.toString()
            itemEmployeeBinding.txtName.text = employee.name
            itemEmployeeBinding.txtAge.text = employee.age.toString()
            itemEmployeeBinding.txtSalary.text = employee.salary.toString()
            this.id = employee.id
        }

        override fun onClick(v: View?) {
            when (v?.id) {
                itemEmployeeBinding.btnDelete.id -> listener.onBtnDeleteClicked(id)
                itemEmployeeBinding.btnEdit.id -> listener.onBtnEditClicked(id)
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EmployeeViewHolder =
        EmployeeViewHolder(ItemEmployeeBinding.inflate(LayoutInflater.from(parent.context), parent, false), listen)


    override fun onBindViewHolder(holder: EmployeeViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int = items.size
}