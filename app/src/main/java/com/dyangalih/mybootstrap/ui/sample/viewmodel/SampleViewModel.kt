package com.dyangalih.mybootstrap.ui.sample.viewmodel

import androidx.lifecycle.*
import com.dyangalih.mybootstrap.data.entities.SampleListTable
import com.dyangalih.mybootstrap.data.repository.SampleRepository
import com.dyangalih.mybootstrap.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SampleViewModel
@Inject constructor(
    val handle: SavedStateHandle,
    private val sampleRepository: SampleRepository
) : ViewModel() {
    val sample = sampleRepository.getAll()
}