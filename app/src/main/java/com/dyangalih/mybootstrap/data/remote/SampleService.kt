package com.dyangalih.mybootstrap.data.remote

import com.dyangalih.mybootstrap.data.entities.SampleListTable
import com.dyangalih.mybootstrap.data.entities.SampleTable
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.*

interface SampleService {
    @GET("sample")
    suspend fun getAll(): Response<SampleListTable>

    @GET("sample/{id}")
    suspend fun getSingle(@Path("id") id: Int): Response<SampleTable>

    @Multipart
    @POST("sample")
    @Headers("Accept: application/json")
    suspend fun store(
        @Part("name") name: RequestBody?
    ): Response<SampleTable>

    @Multipart
    @POST("sample/{id}/update")
    @Headers("Accept: application/json")
    suspend fun update(
        @Part("name") name: RequestBody?
    ): Response<SampleTable>

    @Multipart
    @POST("sample/{id}/delete")
    @Headers("Accept: application/json")
    suspend fun delete(): Response<SampleTable>
}