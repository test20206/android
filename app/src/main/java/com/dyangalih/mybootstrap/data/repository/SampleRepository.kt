package com.dyangalih.mybootstrap.data.repository

import com.dyangalih.mybootstrap.data.local.SampleDao
import com.dyangalih.mybootstrap.data.remote.SampleRemoteDataSource
import com.dyangalih.mybootstrap.utils.performGetOperation
import okhttp3.RequestBody
import retrofit2.http.Part
import javax.inject.Inject

class SampleRepository @Inject constructor(
    private val remoteDataSource: SampleRemoteDataSource,
    private val localDataSource: SampleDao
) {
    fun getSingle(id: Int) = performGetOperation(
        databaseQuery = { localDataSource.get(id) },
        networkCall = { remoteDataSource.getAll() },
        saveCallResult = { localDataSource.insertAll(it.results) }
    )

    fun getAll() = performGetOperation(
        databaseQuery = { localDataSource.getAll() },
        networkCall = { remoteDataSource.getAll() },
        saveCallResult = { localDataSource.insertAll(it.results) }
    )

    suspend fun store(
        @Part("name") name: RequestBody?
    ) = remoteDataSource.store(name)

    suspend fun update(
        @Part("name") name: RequestBody?
    ) = remoteDataSource.update(name)

    suspend fun delete() = remoteDataSource.delete()

}