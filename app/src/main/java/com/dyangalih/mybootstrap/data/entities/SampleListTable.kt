package com.dyangalih.mybootstrap.data.entities

data class SampleListTable(
    val info: Info,
    val results: List<SampleTable>
)
