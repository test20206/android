package com.dyangalih.mybootstrap.data.remote

import okhttp3.RequestBody
import retrofit2.http.Part
import javax.inject.Inject

class SampleRemoteDataSource @Inject constructor(
    private val sampleService: SampleService
) : BaseDataSource() {
    suspend fun getAll() = getResult { sampleService.getAll() }
    suspend fun getSingle(id: Int) = getResult { sampleService.getSingle(id) }
    suspend fun store(@Part("name") name: RequestBody?) = getResult { sampleService.store(name) }
    suspend fun update(@Part("name") name: RequestBody?) = getResult { sampleService.update(name) }
    suspend fun delete() = getResult { sampleService.delete() }
}