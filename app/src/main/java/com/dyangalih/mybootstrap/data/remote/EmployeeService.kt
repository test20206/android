package com.dyangalih.mybootstrap.data.remote

import com.dyangalih.mybootstrap.data.entities.Employee
import com.dyangalih.mybootstrap.data.entities.EmployeeList
import com.dyangalih.mybootstrap.data.remote.request.EmployeeStoreRequest
import retrofit2.Response
import retrofit2.http.*

interface EmployeeService {
    @GET("employees")
    suspend fun getAll(): Response<EmployeeList>

    @POST("create")
    @Headers("Accept: application/json")
    suspend fun create(@Body employee: EmployeeStoreRequest): Response<Employee>

    @POST("update/{id}")
    @Headers("Accept: application/json")
    suspend fun update(@Body employee: EmployeeStoreRequest, @Path("id") id: Int): Response<Employee>

    @GET("delete/{id}")
    @Headers("Accept: application/json")
    suspend fun delete(@Path("id") id: Int): Response<Employee>

    @GET("employee/{id}")
    @Headers("Accept: application/json")
    suspend fun getById(@Path("id") id: Int): Response<Employee>
}