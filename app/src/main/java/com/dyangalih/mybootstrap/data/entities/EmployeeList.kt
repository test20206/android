package com.dyangalih.mybootstrap.data.entities

data class EmployeeList(
    val info: Info,
    val results: List<Employee>
)
