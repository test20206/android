package com.dyangalih.mybootstrap.data.remote

import com.dyangalih.mybootstrap.data.entities.Employee
import com.dyangalih.mybootstrap.data.remote.request.EmployeeStoreRequest
import retrofit2.http.Body
import javax.inject.Inject

class EmployeeRemoteDataSource @Inject constructor(
    private val employeeService: EmployeeService
): BaseDataSource() {
    suspend fun getAll() = getResult { employeeService.getAll() }
    suspend fun getById(id: Int) = getResult { employeeService.getById(id) }
    suspend fun create(@Body employee: EmployeeStoreRequest) = getResult { employeeService.create(employee) }
    suspend fun update(@Body employee: EmployeeStoreRequest, id: Int) = getResult { employeeService.update(employee, id) }
    suspend fun delete(id: Int) = getResult { employeeService.delete(id) }
}