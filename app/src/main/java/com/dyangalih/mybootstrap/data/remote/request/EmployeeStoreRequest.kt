package com.dyangalih.mybootstrap.data.remote.request

data class EmployeeStoreRequest(
    var name: String,
    val salary: Float,
    val age: Int
)
