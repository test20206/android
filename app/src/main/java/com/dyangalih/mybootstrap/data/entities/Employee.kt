package com.dyangalih.mybootstrap.data.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Employee")
data class Employee(
    @PrimaryKey
    val id: Int,
    val name: String,
    val salary: Float,
    val age: Int
)
