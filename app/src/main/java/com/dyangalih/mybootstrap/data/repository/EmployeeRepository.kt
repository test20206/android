package com.dyangalih.mybootstrap.data.repository

import com.dyangalih.mybootstrap.data.remote.EmployeeRemoteDataSource
import com.dyangalih.mybootstrap.data.remote.request.EmployeeStoreRequest
import retrofit2.Response
import retrofit2.http.Body
import javax.inject.Inject


class EmployeeRepository @Inject constructor(
    private val remoteDataSource: EmployeeRemoteDataSource
){
    suspend fun getEmployee() = remoteDataSource.getAll()
    suspend fun getById(id: Int) = remoteDataSource.getById(id)
    suspend fun create(@Body employee: EmployeeStoreRequest) = remoteDataSource.create(employee)
    suspend fun update(@Body employee: EmployeeStoreRequest, id: Int) = remoteDataSource.update(employee, id)
    suspend fun delete(id: Int) = remoteDataSource.delete(id)
}