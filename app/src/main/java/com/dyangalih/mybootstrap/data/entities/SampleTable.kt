package com.dyangalih.mybootstrap.data.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "samples")
data class SampleTable(
    @PrimaryKey
    val id: Int,
    val name: String
)
