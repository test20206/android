package com.dyangalih.mybootstrap.data.local

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.dyangalih.mybootstrap.data.entities.SampleTable

@Dao
interface SampleDao {
    @Query("SELECT * FROM samples")
    fun getAll() : LiveData<List<SampleTable>>

    @Query("SELECT * FROM samples WHERE id = :id")
    fun get(id: Int): LiveData<SampleTable>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(characters: List<SampleTable>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(table: SampleTable)
}