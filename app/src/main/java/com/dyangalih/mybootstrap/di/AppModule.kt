package com.dyangalih.mybootstrap.di

import android.content.Context
import com.dyangalih.mybootstrap.data.local.AppDatabase
import com.dyangalih.mybootstrap.data.local.SampleDao
import com.dyangalih.mybootstrap.data.remote.EmployeeRemoteDataSource
import com.dyangalih.mybootstrap.data.remote.EmployeeService
import com.dyangalih.mybootstrap.data.remote.SampleRemoteDataSource
import com.dyangalih.mybootstrap.data.remote.SampleService
import com.dyangalih.mybootstrap.data.repository.EmployeeRepository
import com.dyangalih.mybootstrap.data.repository.SampleRepository
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideRetrofit(gson: Gson) : Retrofit = Retrofit.Builder()
        .baseUrl("http://dummy.restapiexample.com/api/v1/")
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()

    @Provides
    fun provideGson(): Gson = GsonBuilder().create()

    @Singleton
    @Provides
    fun provideDatabase(@ApplicationContext applicationContext: Context) = AppDatabase.getDatabase(applicationContext)

    @Singleton
    @Provides
    fun provideSimpleDao(db: AppDatabase) = db.sampleDao()

    @Provides
    fun provideSampleService(retrofit: Retrofit): SampleService = retrofit.create(SampleService::class.java)

    @Singleton
    @Provides
    fun provideSampleRemoteDataSource(sampleService: SampleService) = SampleRemoteDataSource(sampleService)

    @Singleton
    @Provides
    fun provideSampleRepository(sampleRemoteDataSource: SampleRemoteDataSource, sampleLocalDataSource: SampleDao) = SampleRepository(sampleRemoteDataSource, sampleLocalDataSource)

    @Provides
    fun provideEmployeeService(retrofit: Retrofit): EmployeeService = retrofit.create(EmployeeService::class.java)

    @Singleton
    @Provides
    fun provideEmployeeRemoteDataSource(employeeService: EmployeeService) = EmployeeRemoteDataSource(employeeService)

    @Singleton
    @Provides
    fun provideEmployeeRepository(remoteDataSource: EmployeeRemoteDataSource) = EmployeeRepository(remoteDataSource)


}